import * as React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import Category from '../screens/categoryScreen'
import ItemList from '../screens/ItemList'
import AddQuantity from '../screens/QuantityScreen'
import Summary from '../screens/SummaryScreen'
import Print from '../screens/PrintScreen'
import Bill from '../screens/BillScreen'
import Confirm from '../screens/Confirm'
import Breadcumb from '../screens/MainNavgator'

const Stack = createStackNavigator();

function Navigator() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName='Category'>
                <Stack.Screen
                    name='Category'
                    component={Category}
                    options={{ title: 'FoodOrderApp' }} />
                <Stack.Screen name='ItemList' component={ItemList} />
                <Stack.Screen name='AddQuantity' component={AddQuantity} />
                <Stack.Screen name='Summary' component={Summary} />
                <Stack.Screen name='Bill Print' component={Print} />
                <Stack.Screen name='Bills' component={Bill} />
                <Stack.Screen name='Confirm' component={Confirm} />
                <Stack.Screen name='Breadcumb' component={Breadcumb} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Navigator;
import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { FlatGrid } from 'react-native-super-grid';

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

const items = [
    { code: '#1abc9c' }, { code: '#2ecc71' },
    { code: '#3498db' }, { code: '#9b59b6' },
]

function CategoryTile(props) {

    let min = 1;
    let max = 10;
    let rand = min + (Math.random() * (max - min));
    console.log(rand);


    return (
        <View style={styles.gridItem}>

            <TouchableOpacity
                style={{ flex: 1 }}
                onPress={props.onSelect}
            >

                <View style={styles.container}>
                    <View style={styles.icons}>
                        <Icon name='cart-plus' size={100} />
                    </View>
                    <Text style={styles.title}>
                        {props.itemTitle}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        margin: 15,
        height: 150,
        borderRadius: 10,
        overflow: 'hidden'
    },
    icons: {
        justifyContent: 'flex-start',
        alignItems: 'stretch'
    },
    container: {
        flex: 1,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 22,
        textAlign: 'center'
    }

});

export default CategoryTile
import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';


function ItemRow(props) {

    const [dataMenuDish, setDataMenuDish] = useState([])

    // let dataMenuDish = []

    useEffect(() => {

        var config = {
            method: 'get',
            url: `http://cheesebin.com/administrator/index.cfm?p=dishmenu/MenuDish&method=MenuDishSearch&id=${props.menu_dishid}`,
            headers: {
                'Content-Type': 'application/json',
                'Cookie': 'cfid=f691028f-03ba-4abe-8dd4-2c605c231fe7; cftoken=0'
            }
        };
        console.log("in config");
        axios(config)
            .then(function (response) {

                setDataMenuDish(response.data.data)
                // dataMenuDish = response.data.data;
                //console.log(JSON.stringify(response.data))
            })
            .catch(function (error) {
                console.log(error);
            });

    }, [])

    return (
        <View style={styles.container}>
            <Image source={{ uri: 'https://image.shutterstock.com/image-vector/red-dish-like-food-delivery-260nw-288348197.jpg' }}
                style={styles.photo} />
            <Text style={[styles.text, { color: '#fff' }]}>
                {`${props.menu_dishid} \nQuantity: ${props.qty}`}
            </Text>
            <TouchableOpacity style={styles.icon}>
                <Icon style={styles.icon} name='close' color='#FF0000' size={30} />
            </TouchableOpacity>

        </View>

    )
}

export default ItemRow

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#4169e1',
        borderRadius: 20,
        margin: 5
    },
    text: {

        marginLeft: 20,
        fontSize: 16,
    },
    photo: {
        height: 40,
        width: 40,
        borderRadius: 20,
    },
    icon: {
        width: 40,
        marginLeft: 10,

    }
});




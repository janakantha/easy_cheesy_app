import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Card, ListItem, Button } from 'react-native-elements'

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];


//<Text>{JSON.stringify(dataMenuDish)}</Text>
function MenuDishItem(props) {
    return (
        <View>
            <TouchableOpacity style={styles.container}
                onPress={props.onAddCart}>
                <Image source={{ uri: 'https://image.shutterstock.com/image-vector/red-dish-like-food-delivery-260nw-288348197.jpg' }}
                    style={styles.photo} />
                <Text style={[styles.text, { color: '#fff' }]}>
                    {`${props.itemData.MenuDishName} \nPrice : ${props.itemData.Price}`}
                </Text>

            </TouchableOpacity>
        </View>

    )
}

export default MenuDishItem;

const styles = StyleSheet.create({

    container: {
        flex: 1,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: '#4169e1',
        borderRadius: 20,
        margin: 5
    },
    text: {
        marginLeft: 20,
        fontSize: 16,
    },
    photo: {
        height: 40,
        width: 40,
        borderRadius: 20,
    },
    icon: {
        width: 40,
        marginLeft: 20
    }
});
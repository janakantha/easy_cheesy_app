import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'


console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];


function QtyButtonTile(props) {
    return (
        <View style={styles.gridItem}>

            <TouchableOpacity
                style={{ flex: 1 }}
                onPress={props.onSelect}
            >
                <View style={styles.container}>

                    <Text style={[styles.title, { color: '#fff' }]}>
                        {props.btnTitle}
                    </Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}
const styles = StyleSheet.create({
    gridItem: {
        flex: 1,
        margin: 15,
        height: 100,
        backgroundColor: '#4169e1',
        borderRadius: 10,
        overflow: 'hidden'
    },
    icons: {
        justifyContent: 'flex-start',
        alignItems: 'stretch'
    },
    container: {
        flex: 1,
        borderRadius: 10,
        shadowColor: 'black',
        shadowOpacity: 0.26,
        shadowOffset: { width: 0, height: 2 },
        shadowRadius: 10,
        elevation: 3,
        padding: 5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 52,
        textAlign: 'center'
    }

});
export default QtyButtonTile

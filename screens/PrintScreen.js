import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, ScrollView, TouchableOpacity, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

function PrintScreen(props) {
    const dataCart_Name = 'loc_Cart';
    const [myCart, setMyCart] = useState([]);

    useEffect(() => {
        AsyncStorage.getItem(dataCart_Name).then((myCart) => {

            if (myCart !== null) {

                setMyCart(JSON.parse(myCart))
            }

        }).catch((err) => {
            console.log(err);
        })
    }, [])

    return (
        <View style={styles.main}>
            <Text style={[styles.title, { color: '#fff' }]}>Invoice Bill</Text>
            <ScrollView style={styles.sheet}>

                <Text style={styles.subtitle}>Item Name | Added QTY | Price</Text>
                <FlatList
                    renderItem={({ item }) => <Text style={styles.subtitle} >
                        {`      ${item.menu_dishid}         |         ${item.qty}          |     N/A`}
                    </Text>}
                    data={myCart}
                    numColumns='1'
                    keyExtractor={(item, index) => item.MenuID} />
                <Text style={styles.subtitle}>                  Invoice Total   :    0.00</Text>
                <Text style={styles.subtitle}>                        Payment    :    0.00</Text>
            </ScrollView>
            <TouchableOpacity style={styles.btn}
                onPress={() => {
                    // props.navigation.navigate('Bill Print');
                    AsyncStorage.getAllKeys().then(AsyncStorage.multiRemove)
                }} >
                <Text style={styles.text}>Print</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => {
                props.navigation.navigate('Category');
            }}>
                <Text style={styles.text}>Back To Home</Text>
            </TouchableOpacity>
        </View>
    )
}

export default PrintScreen

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#191970',
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    text: {
        textAlign: 'center',

        fontSize: 20,
    },
    sheet: {
        margin: 10,
        height: 200,
        backgroundColor: '#fff',
        borderRadius: 20
    },
    btn: {
        borderWidth: 2,
        padding: 5,
        backgroundColor: '#00bfff',
        margin: 10,
        borderRadius: 20
    },
    title: {
        textAlign: 'center',
        fontSize: 30,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
    subtitle: {
        fontSize: 20,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
})
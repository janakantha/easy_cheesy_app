import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, TouchableOpacity, Image, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

function Confirm(props) {

    const dataCart_Name = 'loc_Cart';
    useEffect(() => {

        const cartDetail = {
            menu_dishid: props.route.params.menu_dishid,
            qty: props.route.params.addedQty,
        }
        AsyncStorage.getItem(dataCart_Name).then((myCart) => {
            console.log('IN');
            if (myCart !== null) {
                console.log('update');
                const cart = JSON.parse(myCart);
                cart.push(cartDetail);

                AsyncStorage.setItem(dataCart_Name, JSON.stringify(cart));
            } else {
                console.log('insert');
                const cart = [];
                cart.push(cartDetail);

                AsyncStorage.setItem(dataCart_Name, JSON.stringify(cart));
            }
            console.log(JSON.parse(myCart))
        }).catch((err) => {
            console.log(err);
        })
    }, [])

    return (
        <View style={styles.main}>
            <View style={styles.container}>
                <Image source={{ uri: 'https://image.shutterstock.com/image-vector/red-dish-like-food-delivery-260nw-288348197.jpg' }}
                    style={styles.photo} />
                <Text style={[styles.txt, { color: '#fff' }]}>Id {props.route.params.menu_dishid} Added</Text>
                <Text style={[styles.txt, { color: '#fff' }]}>Quantity :{props.route.params.addedQty} </Text>
                <Text style={[styles.txt, { color: '#fff' }]}>Price : 0.00 </Text>
                <View style={styles.btns} >
                    <TouchableOpacity style={styles.btn} onPress={() => {

                        props.navigation.navigate('Bill Print')
                    }}>
                        <Text style={styles.text}>Buy & Confirm</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => {

                        props.navigation.navigate('Category')

                    }} >
                        <Text style={styles.text}>Confirm & Continue</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

export default Confirm
const styles = StyleSheet.create({
    main: {
        backgroundColor: '#191970',
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    txt: {

        fontSize: 24
    },
    container: {
        marginTop: 50,
        padding: 12,
        alignItems: 'center',
        backgroundColor: '#4169e1',
        borderRadius: 20,
        margin: 10

    },
    btns: {
        marginTop: 20,
        flexDirection: 'column'
    },
    btn: {

        marginBottom: 20,
        borderWidth: 2,
        padding: 10,
        backgroundColor: '#00bfff',

        borderRadius: 20
    },
    text: {
        textAlign: 'center',
        marginLeft: 20,
        fontSize: 22,
    },
    photo: {
        height: 80,
        width: 80,
        borderRadius: 20,
    },
    icon: {
        width: 40,
        marginLeft: 20
    }
});
import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Dimensions, Image } from 'react-native'
import axios from 'axios';
import { FlatGrid } from 'react-native-super-grid';
import Breadcrumb from 'react-native-breadcrumb';

//import CategoryTile from '../components/CategoryTile'


console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

function categoryScreen(props) {

    const moreBtns = [{ "MenuName": "Cart", "MenuID": -1 }, { "MenuName": "Bills", "MenuID": -2 }]

    const items = [
        { code: '#1abc9c' }, { code: '#2ecc71' },
        { code: '#3498db' }, { code: '#9b59b6' },
        { code: '#ff5349' }, { code: '#cc3333' },
        { code: '#ffa500' }, { code: '#32cd32' },
        { code: '#708090' }, { code: '#00ced1' },
    ]

    const urls =
    {
        name: 'Cake',
        icon: 'https://linkpicture.com/q/Dinner.png'
    }

    const [dataCategories, setDataCategories] = useState([])

    useEffect(() => {

        var config = {
            method: 'get',
            url: `http://cheesebin.com/administrator/index.cfm?p=dishmenu/Menu&method=list`,
            headers: {
                'Content-Type': 'application/json',
                'Cookie': 'cfid=f691028f-03ba-4abe-8dd4-2c605c231fe7; cftoken=0'
            }
        };

        axios(config)
            .then(function (response) {

                let apiData = [];

                // console.log(JSON.stringify(response.data))
                apiData = response.data;
                moreBtns.map((btn) => {
                    apiData.push(btn)
                })

                setDataCategories(apiData);
                //console.log(apiData)

                // setDataCategories(dataCategories => ({
                //     ...dataCategories,[{ "MenuName": "Cart", "MenuID": 1 },
                // { "MenuName": "Bills", "MenuID": 2 }]
                // }));

            })
            .catch(function (error) {
                console.log(error);
            });


    }, [])

    function categoryRender(item) {

        let colorCode = items[Math.floor(Math.random() * Math.floor(9))].code

        let url = `https://www.linkpicture.com/q/${item.MenuName}.png`;

        return (
            <TouchableOpacity style={[styles.itemContainer, { backgroundColor: `${colorCode}` }]}
                onPress={() => {
                    if (item.MenuID < 0) {
                        if (item.MenuName == 'Cart') {
                            props.navigation.navigate('Summary')
                        } else if (item.MenuName == 'Bills') {
                            props.navigation.navigate('')
                        }
                    } else {

                        props.navigation.navigate('ItemList', { menu_id: item.MenuID })

                    }
                }}>

                <Image style={styles.img} source={{ uri: url }} />
                <Text style={styles.itemName}>{item.MenuName}</Text>
            </TouchableOpacity>
        )
    }


    return (
        <View style={styles.main}>
            <Breadcrumb
                entities={['Category', 'Item', 'Quantity', 'Confirm']}
                isTouchable={true}
                flowDepth={0}
                height={30}
                onCrumbPress={index => { }}
                borderRadius={5}
            />
            <ScrollView>
                <FlatGrid
                    itemDimension={130}
                    data={dataCategories}
                    //data={btns}
                    style={styles.gridView}
                    spacing={15}
                    renderItem={({ item }) => (
                        categoryRender(item)
                    )}
                />

            </ScrollView>

        </View>
    )
}

export default categoryScreen



const styles = StyleSheet.create({
    main: {
        backgroundColor: '#191970',
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    gridView: {
        marginTop: 20,
        flex: 1,
    },
    itemContainer: {
        justifyContent: 'flex-end',
        borderRadius: 5,
        padding: 10,
        height: 150,
    },
    itemName: {
        textAlign: 'center',
        fontSize: 16,
        color: '#fff',
        fontWeight: '600',
    },

    itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
    },
    img: {
        width: 90,
        height: 90,
        alignSelf: 'center'
    }
});
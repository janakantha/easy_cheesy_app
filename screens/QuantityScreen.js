import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, ScrollView, Button, Dimensions } from 'react-native'


import QTYButton from '../components/QtyButtonTile'

function QuantityScreen(props) {

    const [dataButtons, setDataButtons] = useState([1, 2, 3, 4, 5, 6])

    function ButtonRender(item) {

        return (
            <QTYButton
                btnTitle={item}
                onSelect={() => {
                    props.navigation.navigate('Confirm',
                        {
                            menu_dishid: props.route.params.menu_dishid,
                            addedQty: item,
                        })
                    // {props.route.params.menu_dishid}
                }}
            />)
    }
    return (
        <View style={styles.main}>
            <Text style={[styles.title, { color: '#fff' }]}> Select a Quantity </Text>
            <FlatList
                renderItem={({ item }) => ButtonRender(item)}
                data={dataButtons}
                numColumns='2'
                keyExtractor={(item, index) => item.MenuID} />
        </View>
    )
}

export default QuantityScreen
const styles = StyleSheet.create({
    main: {
        backgroundColor: '#191970',
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    }, title: {
        textAlign: 'center',
        fontSize: 30,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
});
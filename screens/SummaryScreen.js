import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, ScrollView, TouchableOpacity, Dimensions } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';

import ItemRow from '../components/ItemRow'

function SummaryScreen(props) {

    const dataCart_Name = 'loc_Cart';
    const [myCart, setMyCart] = useState([]);

    useEffect(() => {
        AsyncStorage.getItem(dataCart_Name).then((myCart) => {

            if (myCart !== null) {

                setMyCart(JSON.parse(myCart))
            }

        }).catch((err) => {
            console.log(err);
        })
    }, [])

    return (
        <View style={styles.main}>
            {/* <Text>{JSON.stringify(myCart)}</Text> */}
            <Text style={[styles.text, { color: '#fff' }]}>Cart Item List</Text>
            <ScrollView>
                <FlatList
                    renderItem={({ item }) => <ItemRow {...item} />}
                    data={myCart}
                    numColumns='1'
                    keyExtractor={(item, index) => item.MenuID} />
                <View style={styles.btns} >
                    <TouchableOpacity style={styles.btn} onPress={() => {

                        props.navigation.navigate('Bill Print')
                    }}>
                        <Text style={[styles.text, { color: '#fff' }]}>Buy & Print</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btn} onPress={() => {

                        props.navigation.navigate('Category')
                    }} >
                        <Text style={[styles.text, { color: '#fff' }]}>Continue To Menu</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

export default SummaryScreen

const styles = StyleSheet.create({
    main: {
        backgroundColor: '#191970',
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height
    },
    txt: {
        fontSize: 24
    },
    container: {
        marginTop: 50,
        padding: 12,
        alignItems: 'center',
        backgroundColor: '#4169e1',
        borderRadius: 20,
        margin: 10
    },
    btns: {
        marginTop: 20,
        flexDirection: 'column'
    },
    btn: {
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 10,
        borderWidth: 2,
        textAlign: 'center',
        paddingTop: 2,
        backgroundColor: '#00bfff',
        borderRadius: 20
    },
    text: {
        textAlign: 'center',
        marginLeft: 20,
        fontSize: 22,
        marginBottom: 10
    },
    photo: {
        height: 80,
        width: 80,
        borderRadius: 20,
    },
    icon: {
        width: 40,
        marginLeft: 20
    }
});
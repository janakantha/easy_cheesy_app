import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, ScrollView, Dimensions } from 'react-native'
import axios from 'axios';
import MenuDish from '../components/MenuDishItem'

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

function ItemList(props) {

    const [dataMenuDish, setDataMenuDish] = useState([])
    useEffect(() => {

        var config = {
            method: 'get',
            url: `http://cheesebin.com/administrator/index.cfm?p=dishmenu/MenuDish&method=MenuDishSearch&id=${props.route.params.menu_id}`,
            headers: {
                'Content-Type': 'application/json',
                'Cookie': 'cfid=f691028f-03ba-4abe-8dd4-2c605c231fe7; cftoken=0'
            }
        };

        axios(config)
            .then(function (response) {

                setDataMenuDish(response.data.data)
                //console.log(JSON.stringify(response.data))
            })
            .catch(function (error) {
                console.log(error);
            });

    }, [])

    function renderMenus(item) {
        return (
            <View>

                <MenuDish
                    itemData={item}
                    onAddCart={() => {
                        props.navigation.navigate('AddQuantity', { menu_dishid: item.MenuDishID })
                        // console.log('render method')
                    }

                    }
                />
            </View>
        );
    }

    return (
        <View style={styles.main}>
            <Text style={[styles.title, { color: '#fff' }]}>Food List</Text>
            <ScrollView>
                <FlatList
                    horizontal={false}
                    data={dataMenuDish}
                    renderItem={({ item }) => renderMenus(item)}
                    keyExtractor={(item, index) => index.toString()}
                />
            </ScrollView>
        </View>
    )
}

export default ItemList
const styles = StyleSheet.create({
    main: {
        backgroundColor: '#191970',
        flex: 1,
        width: Dimensions.get('window').width
    }, title: {
        textAlign: 'center',
        fontSize: 30,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
    },
});
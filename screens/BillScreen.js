import React, { useState, useEffect } from 'react'
import { View, Text, StyleSheet, FlatList, ScrollView, Button, TouchableOpacity } from 'react-native'

function BillScreen() {
    return (
        <View>
            <Text style={{ fontSize: 30 }}>Previous Bills </Text>
            <TouchableOpacity style={styles.container} >
                <Text> Bill Date :</Text>
                <Text>2020/09/25  </Text>
                <Text>Invoice Total : </Text>
                <Text>0.00</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.container} >
                <Text> Bill Date :</Text>
                <Text>2020/09/26  </Text>
                <Text>Invoice Total : </Text>
                <Text>0.00</Text>
            </TouchableOpacity>
        </View>
    )
}

export default BillScreen
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#D3D3D3',
        flex: 1,
        marginTop: 10,
        padding: 12,
        flexDirection: 'row',
        alignItems: 'center',
        height: 300
    },
})
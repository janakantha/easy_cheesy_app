import React from 'react';
import Breadcrumb from 'react-native-breadcrumb';

function MainNavgator() {
    return (
        <Breadcrumb
            entities={['My Tab 1', 'My Tab 2', 'My Tab 3', 'my pp']}
            isTouchable={true}
            flowDepth={2}
            height={30}
            onCrumbPress={index => { }}
            borderRadius={5}
        />
    )
}

export default MainNavgator
